//
// Created by Dan on 18.10.2021.
//

#include "AvhInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    avh_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
